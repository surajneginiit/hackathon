package com.example.hackathont3

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class HackathonApplication: Application() {
}