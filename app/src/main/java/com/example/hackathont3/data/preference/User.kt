package com.example.hackathont3.data.preference

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "user")

class AuthPreference @Inject constructor(
    @ApplicationContext private val context: Context
) {


    suspend fun setUserId(userId: String?) {
        context.dataStore.edit { settings ->
            settings[USER_ID] = userId?: ""
        }
    }

    suspend fun setDatabaseUrl(databaseUrl: String?){
        context.dataStore.edit { settings->
            settings[DATABASE_URL] = databaseUrl?: ""
        }
    }

    suspend fun setVisitFirst(isVisited: Boolean){
        context.dataStore.edit { settings->
            settings[FIRSTVISIT] = isVisited
        }
    }

    suspend fun setWebviewUrl(url: String){
        context.dataStore.edit { settings->
            settings[WEBVIEW_URL] = url
        }
    }

    val visitFirst: Flow<Boolean?> =context.dataStore.data
        .map { preferences ->
            preferences[FIRSTVISIT]
        }

    val userId: Flow<String?> = context.dataStore.data
        .map { preferences ->
            // No type safety.
            preferences[USER_ID]
        }

    val databaseUrl: Flow<String?> =context.dataStore.data
        .map { preferences ->

            preferences[DATABASE_URL]
        }
    val webviewUrl: Flow<String?> =context.dataStore.data
        .map { preferences ->

            preferences[WEBVIEW_URL]
        }
    companion object {
        val USER_ID = stringPreferencesKey("user_id")
        val DATABASE_URL = stringPreferencesKey("database_url")
        val WEBVIEW_URL = stringPreferencesKey("webview_url")
        val FIRSTVISIT = booleanPreferencesKey("first_visit")
    }

}
